#ifndef __DSP_MMU_UTIL_INCLUDED
#define __DSP_MMU_UTIL_INCLUDED
//     ----------------------------------------------------------------------
//
//                  OMAP3530 DSP MMU utility functions.
//                copyright and written by Nils Pipenbrinck 2010
//
//                This code is released under the BSD license.
//
//     ----------------------------------------------------------------------
//
//      Redistribution and use in source and binary forms, with or without
//      modification, are permitted provided that the following conditions are
//      met:
//
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above
//        copyright notice, this list of conditions and the following disclaimer
//        in the documentation and/or other materials provided with the
//        distribution.
//      * Neither the name of the  nor the names of its
//        contributors may be used to endorse or promote products derived from
//        this software without specific prior written permission.
//
//      THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//      "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//      LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//      A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//      OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//      SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//      LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//      DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//      THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//      (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//      OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


//
//   Note: All functions return NonZero on success.
//



// Maps a physical memory region into the DSP address-space
int dsp_mmu_map (unsigned long physical_ptr, int size);


// Unmaps a physical memory region out of the DSP address-space
int dsp_mmu_unmap (unsigned long physical_ptr, int size);


// Map the first CMEM block into the DSP address space
int dsp_mmu_map_cmem (void);


// Unmap the first CMEM block out of the DSP address space
int dsp_mmu_unmap_cmem (void);


// Disables the DSP MMU. Sets virtual = physical mapping.
//
// Call this between PROC_Load and PROC_start.
//
// Warning: This function removes *all* protection.
//          With a disabled MMU you can accidently
//          erase the flash and do other nasty things.
//
int dsp_mmu_disable (void);

#endif
