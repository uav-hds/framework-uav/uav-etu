/** ============================================================================
 *  @file   message.h
 *
 *  @path   $(DSPLINK)/gpp/src/samples/message/
 *
 *  @desc   Defines the configurable parameters for the message test which
 *          sends a message across the DSP processor and receives it back
 *          using DSP/BIOS LINK.
 *          It also does the data verification on the received message.
 *
 *  @ver    1.63
 *  ============================================================================
 *  Copyright (C) 2002-2009, Texas Instruments Incorporated -
 *  http://www.ti.com/
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *  *  Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *  *  Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *  *  Neither the name of Texas Instruments Incorporated nor the names of
 *     its contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 *  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 *  PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 *  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 *  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ============================================================================
 */

#if !defined (MESSAGE_H)
#define MESSAGE_H

#include <cxtypes.h>


#if defined (__cplusplus)
extern "C" {
#endif /* defined (__cplusplus) */



// Configuration Macros

// Messaging buffer used by the application.
// Note: This buffer must be aligned according to the alignment expected
// by the device/platform.
#define APP_OUT_BUFFER_SIZE DSPLINK_ALIGN (sizeof (DSPCV_query), DSPLINK_BUF_ALIGN)
//#define APP_IN_BUFFER_SIZE DSPLINK_ALIGN (sizeof (DSPLIB_result), DSPLINK_BUF_ALIGN)
/** ============================================================================
 *  @func   MESSAGE_Create
 *
 *  @desc   This function allocates and initializes resources used by
 *          this application.
 *
 *  @arg    dspExecutable
 *              DSP executable name.
 *  @arg    strNumIterations
 *              Number of iterations for which a message is transferred between
 *              GPP and DSP in string format.
 *  @arg    processorId
 *             Id of the DSP Processor.
 *
 *  @ret    DSP_SOK
 *              Operation successfully completed.
 *          DSP_EFAIL
 *              Resource allocation failed.
 *
 *  @enter  None
 *
 *  @leave  None
 *
 *  @see    MESSAGE_Delete
 *  ============================================================================
 */
NORMAL_API
Void
init_dsp(IN Char8 * dspExecutable) ;


/** ============================================================================
 *  @func   MESSAGE_Execute
 *
 *  @desc   This function implements the execute phase for this application.
 *
 *  @arg    numIterations
 *              Number of times to send the message to the DSP.
 *
 *  @arg    processorId
 *             Id of the DSP Processor.
 *
 *  @ret    DSP_SOK
 *              Operation successfully completed.
 *          DSP_EFAIL
 *              MESSAGE execution failed.
 *
 *  @enter  None
 *
 *  @leave  None
 *
 *  @see    MESSAGE_Delete , MESSAGE_Create
 *  ============================================================================
 */

NORMAL_API
DSP_STATUS
MESSAGE_Execute(IN DSPCV_query* outMsg);

NORMAL_API
DSP_STATUS
receive_message(void);

NORMAL_API
DSP_STATUS
send_message(IN DSPCV_query* outMsg);


/** ============================================================================
 *  @func   MESSAGE_Delete
 *
 *  @desc   This function releases resources allocated earlier by call to
 *          MESSAGE_Create ().
 *          During cleanup, the allocated resources are being freed
 *          unconditionally. Actual applications may require stricter check
 *          against return values for robustness.
 *
 *  @arg    processorId
 *             Id of the DSP Processor.
 *
 *  @ret    DSP_SOK
 *              Operation successfully completed.
 *          DSP_EFAIL
 *              Resource deallocation failed.
 *
 *  @enter  None
 *
 *  @leave  None
 *
 *  @see    MESSAGE_Create
 *  ============================================================================
 */
NORMAL_API
Void
close_dsp() ;


/** ============================================================================
 *  affichage du debug DSP
 *
 *  ============================================================================
 */

Void LogCallback (Uint32 event, Pvoid arg, Pvoid info);


/** ============================================================================
 *  fonctions OpenCV
 *
 *  ============================================================================
 */
NORMAL_API
char*
dsptestjpeg(IN IplImage* src_img,unsigned int* compressed_size);


NORMAL_API
Void
dspGoodFeaturesToTrack(IN IplImage* img_src,IN CvPoint* features,IN unsigned int* count,float quality_level, float min_distance);


NORMAL_API
Void
dspPyrDown(IN IplImage* img_src,IN IplImage* img_dst,IN Uint8 level);


NORMAL_API
Void
dspCalcOpticalFlowPyrLK(IN IplImage* img_A,IN IplImage* img_B,IN IplImage* pyr_A,IN IplImage* pyr_B,
        IN CvPoint* features_A,IN CvPoint* features_B,IN int count,IN CvSize winSize,
        IN int level,IN char *feat_status,IN unsigned int *error,IN CvTermCriteria criteria,IN int flags);

NORMAL_API
Void
dspCvtColor(IN IplImage* img_src,IN IplImage* img_dst, int code);

NORMAL_API
Void
dspThreshold(IN IplImage* img_src,IN IplImage* img_dst, float threshold,float max_value,int threshold_type );

NORMAL_API
Void
dspCloneImage(IN IplImage* img_src,IN IplImage* img_dst);

NORMAL_API
Void
dspSobel(IN IplImage* img_src,IN IplImage* img_dst,int dx,int dy);

NORMAL_API
int
dspHoughLines2(IN IplImage* img_src, CvMat* line_storage, int method,
                              float rho, float theta, int threshold,
                              float param1=0, float param2=0);


#if defined (__cplusplus)
}
#endif /* defined (__cplusplus) */


#endif /* !defined (MESSAGE_H) */
