/** ============================================================================
 *  @file   message.c
 *
 *  @path   $(DSPLINK)/gpp/src/samples/message/
 *
 *  @desc   This is an application which sends messages to the DSP
 *          processor and receives them back using DSP/BIOS LINK.
 *          The message contents received are verified against the data
 *          sent to DSP.
 *
 *  @ver    1.63
 *  ============================================================================
 *  Copyright (C) 2002-2009, Texas Instruments Incorporated -
 *  http://www.ti.com/
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *  *  Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *  *  Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *  *  Neither the name of Texas Instruments Incorporated nor the names of
 *     its contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 *  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 *  PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 *  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 *  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ============================================================================
 */


#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

/*  ----------------------------------- DSP/BIOS Link                   */
#include <dsplink.h>

/*  ----------------------------------- DSP/BIOS LINK API               */
#include <proc.h>
#include <msgq.h>
#include <pool.h>
#include <notify.h>

/*  ----------------------------------- CMEM               */
#include <cmem.h>
#include "dsp_mmu_util.h"

/*  ----------------------------------- OpenCV               */
#include <cxcore.h>
#include "opencv_common.h"
#include "opencv_dsp.h"

/*  ----------------------------------- Application Header              */
#include "message.h"
#include "message_os.h"

DSP_STATUS status=DSP_SOK ;

//#if defined (__cplusplus)
//extern "C" {
//#endif /* defined (__cplusplus) */

#define DSP_ID 0


/** ============================================================================
 *  @name   SAMPLE_POOL_ID
 *
 *  @desc   ID of the POOL used for the sample.
 *  ============================================================================
 */
#define SAMPLE_POOL_ID       0

/** ============================================================================
 *  @const  NUMMSGPOOLS
 *
 *  @desc   Number of BUF pools in the pool
 *  ============================================================================
 */
#define NUMMSGPOOLS     4

/** ============================================================================
 *  @const  NUMMSGINPOOL0
 *
 *  @desc   Number in first message pool.
 *  ============================================================================
 */
#define NUMMSGINPOOL0   1

/** ============================================================================
 *  @const  NUMMSGINPOOL1
 *
 *  @desc   Number in second message pool.
 *  ============================================================================
 */
#define NUMMSGINPOOL1   2

/** ============================================================================
 *  @const  NUMMSGINPOOL2
 *
 *  @desc   Number in third message pool.
 *  ============================================================================
 */
#define NUMMSGINPOOL2   2

/** ============================================================================
 *  @const  NUMMSGINPOOL3
 *
 *  @desc   Number in fourth message pool.
 *  ============================================================================
 */
#define NUMMSGINPOOL3   4

/** ============================================================================
 *  @const  APP_BUFFER_SIZE
 *
 *  @desc   Messaging buffer used by the application.
 *          Note: This buffer is aligned according to the alignment expected
 *          by the platform.
 *  ============================================================================
 */
#define APP_BUFFER_SIZE  DSPLINK_ALIGN (sizeof (MSGQ_MsgHeader),       \
                                        DSPLINK_BUF_ALIGN)


/** ============================================================================
 *  Definitions required for the sample that vary based on physical link
 *  being used for the MQT.
 *  ============================================================================
 */
#if defined ZCPY_LINK
#define SAMPLEMQT_CTRLMSG_SIZE  ZCPYMQT_CTRLMSG_SIZE
STATIC ZCPYMQT_Attrs  mqtAttrs ;
#endif /* if defined ZCPY_LINK */

/** ============================================================================
 *  @name   SampleBufSizes
 *
 *  @desc   Message sizes managed by the pool.
 *  ============================================================================
 */
STATIC Uint32 SampleBufSizes [NUMMSGPOOLS] =
{
    APP_BUFFER_SIZE,
    SAMPLEMQT_CTRLMSG_SIZE,
    DSPLINK_ALIGN (sizeof (MSGQ_AsyncLocateMsg), DSPLINK_BUF_ALIGN),
    DSPLINK_ALIGN (sizeof (MSGQ_AsyncErrorMsg), DSPLINK_BUF_ALIGN)
} ;

/** ============================================================================
 *  @name   SampleNumBuffers
 *
 *  @desc   Number of messages in each pool managed by the pool.
 *  ============================================================================
 */
STATIC Uint32 SampleNumBuffers [NUMMSGPOOLS] =
{
    NUMMSGINPOOL0,
    NUMMSGINPOOL1,
    NUMMSGINPOOL2,
    NUMMSGINPOOL3
} ;


/** ============================================================================
 *  @name   SamplePoolAttrs
 *
 *  @desc   Definition of attributes for the pool based on physical link used
 *          by the MQT.
 *  ============================================================================
 */
#if defined ZCPY_LINK
STATIC SMAPOOL_Attrs SamplePoolAttrs =
{
    NUMMSGPOOLS,
    SampleBufSizes,
    SampleNumBuffers,
    TRUE
} ;
#endif /* if defined ZCPY_LINK */

/** ============================================================================
 *  @const  MsgqGpp1
 *
 *  @desc   Name of the first MSGQ on the GPP.
 *  ============================================================================
 */
STATIC Char8 SampleGppMsgqName [DSP_MAX_STRLEN] = "GPPMSGQ1" ;

/** ============================================================================
 *  @const  MsgqDsp1
 *
 *  @desc   Name of the first MSGQ on the DSP.
 *  ============================================================================
 */
STATIC Char8 SampleDspMsgqName [DSP_MAX_STRLEN] = "DSPMSGQ0" ;

/** ============================================================================
 *  @name   SampleGppMsgq
 *
 *  @desc   Local GPP's MSGQ Object.
 *  ============================================================================
 */
STATIC MSGQ_Queue SampleGppMsgq = (Uint32) MSGQ_INVALIDMSGQ ;

/** ============================================================================
 *  @name   SampleDspMsgq
 *
 *  @desc   DSP's MSGQ Object.
 *  ============================================================================
 */
STATIC MSGQ_Queue SampleDspMsgq = (Uint32) MSGQ_INVALIDMSGQ ;


/** ============================================================================
 *  @name   LINKCFG_config
 *
 *  @desc   Extern declaration to the default DSP/BIOS LINK configuration
 *          structure.
 *  ============================================================================
 */
extern  LINKCFG_Object LINKCFG_config ;


/** ============================================================================
 *  @func   init_dsp
 *
 *  @desc   This function allocates and initializes resources used by
 *          this application.
 *
 *  @modif  MESSAGE_InpBufs , MESSAGE_OutBufs
 *  ============================================================================
 */
NORMAL_API
Void
init_dsp(IN Char8 * dspExecutable)
{
    MSGQ_LocateAttrs syncLocateAttrs ;
    MSGQ_Msg    msg ;
    //Uint16      msgId= 0 ;
    CMEM_AllocParams params;

    MESSAGE_0Print ("Entered MESSAGE_Create ()\n") ;

    //  Create and initialize the proc object.
    status = PROC_setup (NULL) ;


    // Attach the Dsp with which the transfers have to be done.
    if (DSP_SUCCEEDED (status)) {
        status = PROC_attach (DSP_ID, NULL) ;
        if (DSP_FAILED (status)) {
            printf ("PROC_attach () failed. Status = [0x%x]\n",status) ;
        }
    }

    //Open the pool.
    if (DSP_SUCCEEDED (status)) {
        status = POOL_open (POOL_makePoolId(DSP_ID, SAMPLE_POOL_ID),&SamplePoolAttrs) ;
        if (DSP_FAILED (status)) {
            printf ("POOL_open () failed. Status = [0x%x]\n", status) ;
        }
    }
    else {
        printf ("PROC_setup () failed. Status = [0x%x]\n", status) ;
    }

    //Open the GPP's message queue
    if (DSP_SUCCEEDED (status)) {
        status = MSGQ_open (SampleGppMsgqName, &SampleGppMsgq, NULL) ;
        if (DSP_FAILED (status)) {
            printf ("MSGQ_open () failed. Status = [0x%x]\n",status) ;
        }
    }

    //Set the message queue that will receive any async. errors
    if (DSP_SUCCEEDED (status)) {
        status = MSGQ_setErrorHandler (SampleGppMsgq,POOL_makePoolId(DSP_ID,SAMPLE_POOL_ID)) ;
        if (DSP_FAILED (status)) {
            printf ("MSGQ_setErrorHandler () failed. Status = [0x%x]\n",status) ;
        }
    }

    // Load the executable on the DSP.
    if (DSP_SUCCEEDED (status)) {

        status = PROC_load (DSP_ID, dspExecutable, 0, NULL) ;
        if (DSP_FAILED (status)) {
            printf ("PROC_load () failed. Status = [0x%x]\n", status);
            //return status ;
        }
    }

    // put this between PROC_load and PROC_start in your dsplink init-code:
    //permet affichage debug DSP
    NOTIFY_register(0, 0, 6, LogCallback, 0);

    // Start execution on DSP.
    if (DSP_SUCCEEDED (status)) {
        status = PROC_start (DSP_ID) ;
        if (DSP_FAILED (status)) {
            printf ("PROC_start () failed. Status = [0x%x]\n",status) ;
            //return status ;
        }
    }

    //Open the remote transport.
    if (DSP_SUCCEEDED (status)) {
        mqtAttrs.poolId = POOL_makePoolId(DSP_ID, SAMPLE_POOL_ID)  ;
        status = MSGQ_transportOpen (DSP_ID, &mqtAttrs) ;
        if (DSP_FAILED (status)) {
            printf ("MSGQ_transportOpen () failed. Status = [0x%x]\n",status) ;
        }
     }

    //Locate the DSP's message queue
    if (DSP_SUCCEEDED (status))
    {
        syncLocateAttrs.timeout = WAIT_FOREVER;
        status = DSP_ENOTFOUND ;
        while ((status == DSP_ENOTFOUND) || (status == DSP_ENOTREADY))
        {
            status = MSGQ_locate (SampleDspMsgqName, &SampleDspMsgq,&syncLocateAttrs) ;
            if ((status == DSP_ENOTFOUND) || (status == DSP_ENOTREADY))
            {
                MESSAGE_Sleep (100000) ;
            }
            else if (DSP_FAILED (status))
            {
                printf ("MSGQ_locate () failed. Status = [0x%x]\n",status) ;
            }
        }
    }

    //Receive the first message from dsp
    status = MSGQ_get (SampleGppMsgq, WAIT_FOREVER, &msg) ;
    if (DSP_FAILED (status)) printf ("MSGQ_get () failed. Status = [0x%x]\n", status) ;

    //msgId = MSGQ_getMsgId(msg);
    //MESSAGE_1Print("id recu: %i\n",msgId);
    // Must free the message
    MSGQ_free (msg) ;

    // First initialize the CMEM module
    if (CMEM_init() == -1) printf("Failed to initialize CMEM\n");
    status=dsp_mmu_map_cmem ();

    if (DSP_FAILED (status)) printf ("dsp_mmu_map_cmem () failed. Status = [0x%x]\n", status) ;
    MESSAGE_0Print("CMEM initialized.\n");

    //a regler, les params ne sont pas envoyés au gestionnaire de memoire
    params = CMEM_DEFAULTPARAMS;
    params.flags = CMEM_NONCACHED;
    params.type=CMEM_HEAP;

    //set CMEM as memory manager for opencv
    cvSetMemoryManager(icvCMEMAlloc,icvCMEMFree,&params);

    MESSAGE_0Print ("Leaving MESSAGE_Create ()\n") ;

    //return status ;
}


/** ============================================================================
 *  @func   MESSAGE_Execute
 *
 *  @desc   This function implements the execute phase for this application.
 *
 *  @modif  None
 *  ============================================================================
 */

NORMAL_API
DSP_STATUS
MESSAGE_Execute(IN DSPCV_query* outMsg)
{
    MSGQ_Msg    msg ;

    //MESSAGE_0Print ("Entered MESSAGE_Execute modified()\n") ;


#if defined (MESSAGE_PROFILE)
    MESSAGE_GetStartTime () ;
#endif

    if (DSP_SUCCEEDED (status))
    {
        status = MSGQ_put(SampleDspMsgq, (MsgqMsg) outMsg);
        if (DSP_FAILED (status)) printf ("MSGQ_put () failed. Status = [0x%x]\n", status) ;
     }
     else
     {
      printf("msg non envoyé\n");
     }

    //printf("attente reponse\n");
    //  Receive the message
    status = MSGQ_get (SampleGppMsgq, WAIT_FOREVER, &msg) ;
    if (DSP_FAILED (status)) printf ("MSGQ_get () failed. Status = [0x%x]\n",status);



#if defined (MESSAGE_PROFILE)
    if (DSP_SUCCEEDED (status)) {
        MESSAGE_GetEndTime () ;
        MESSAGE_GetProfileInfo (1) ;
    }
#endif

    status=MSGQ_free (msg) ;
    if (DSP_FAILED (status)) printf ("MSGQ_free () failed. Status = [0x%x]\n",status);


    //MESSAGE_0Print ("Leaving MESSAGE_Execute modified()\n") ;

    return status ;
}

/** ============================================================================
 *  @func   send_message
 *
 *  @desc   This function sends a message to the dsp.
 *
 *  @modif  None
 *  ============================================================================
 */

NORMAL_API
DSP_STATUS
send_message(IN DSPCV_query* outMsg)
{

    //MESSAGE_0Print ("Entered send_message()\n") ;

    if (DSP_SUCCEEDED (status))
    {
        status = MSGQ_put(SampleDspMsgq, (MsgqMsg) outMsg);

        if (DSP_FAILED (status)) printf ("MSGQ_put () failed. Status = [0x%x]\n", status) ;
     }

    //MESSAGE_0Print ("Leaving send_message()\n") ;

    return status ;
}

/** ============================================================================
 *  @func   receive_message
 *
 *  @desc   This function receives a message from the dsp.
            Must be called after send_message.
 *
 *  @modif  None
 *  ============================================================================
 */

NORMAL_API
DSP_STATUS
receive_message(void)
{
    MSGQ_Msg    msg ;

    //MESSAGE_0Print ("Entered receive_message()\n") ;


#if defined (MESSAGE_PROFILE)
    MESSAGE_GetStartTime () ;
#endif

    //printf("attente reponse\n");
    //  Receive the message
    status = MSGQ_get (SampleGppMsgq, WAIT_FOREVER, &msg) ;
    if (DSP_FAILED (status)) printf ("MSGQ_get () failed. Status = [0x%x]\n",status);


#if defined (MESSAGE_PROFILE)
    if (DSP_SUCCEEDED (status)) {
        MESSAGE_GetEndTime () ;
        MESSAGE_GetProfileInfo (1) ;
    }
#endif


    MSGQ_free (msg) ;

    //MESSAGE_0Print ("Leaving MESSAGE_Execute modified()\n") ;

    return status ;
}
/** ============================================================================
 *  @func   close_dsp
 *
 *  @desc   This function releases resources allocated earlier by call to
 *          MESSAGE_Create ().
 *          During cleanup, the allocated resources are being freed
 *          unconditionally. Actual applications may require stricter check
 *          against return values for robustness.
 *
 *  @modif  None
 *  ============================================================================
 */
NORMAL_API
Void
close_dsp()
{
    MESSAGE_0Print ("Entered MESSAGE_Delete ()\n") ;

    //close CMEM
    status =dsp_mmu_unmap_cmem ();
    if (DSP_FAILED (status)) {
        printf ("dsp_mmu_unmap_cmem () failed. Status = [0x%x]\n", status) ;
    }
    if (CMEM_exit() < 0) printf("Failed to finalize the CMEM module\n");


    // Release the remote message queue
    status = MSGQ_release (SampleDspMsgq) ;
    if (DSP_FAILED (status)) {
        printf ("MSGQ_release () failed. Status = [0x%x]\n", status) ;
    }

    //Close the remote transport
    status = MSGQ_transportClose (DSP_ID) ;
    if (DSP_FAILED (status)) {
        printf ("MSGQ_transportClose () failed. Status = [0x%x]\n",
                        status) ;
    }


    //Stop execution on DSP.
    status = PROC_stop (DSP_ID) ;
    if (DSP_FAILED (status)) {
        printf ("PROC_stop () failed. Status = [0x%x]\n", status) ;
    }

    //Reset the error handler before deleting the MSGQ that receives
    //the error messages.
    status = MSGQ_setErrorHandler (MSGQ_INVALIDMSGQ, MSGQ_INVALIDMSGQ) ;
    if ( DSP_FAILED (status)) {
        printf ("MSGQ_setErrorHandler () failed. Status = [0x%x]\n",
                        status) ;
    }

    //Close the GPP's message queue
    status = MSGQ_close (SampleGppMsgq) ;
    if (DSP_FAILED (status)) {
        printf ("MSGQ_close () failed. Status = [0x%x]\n", status) ;
    }

    // Close the pool
    status = POOL_close (POOL_makePoolId(DSP_ID, SAMPLE_POOL_ID)) ;
    if ( DSP_FAILED (status)) {
        printf ("POOL_close () failed. Status = [0x%x]\n", status) ;
    }

    // Detach from the processor
    status = PROC_detach  (DSP_ID) ;
    if ( DSP_FAILED (status)) {
        printf ("PROC_detach () failed. Status = [0x%x]\n", status) ;
    }

    // Destroy the PROC object.
    status = PROC_destroy () ;
    if (DSP_FAILED (status)) {
        printf ("PROC_destroy () failed. Status = [0x%x]\n", status) ;
    }

    //unset CMEM as memory manager for opencv
    cvSetMemoryManager(0,0,NULL);

    MESSAGE_0Print ("Leaving MESSAGE_Delete ()\n") ;
}


/** ============================================================================
 *  affichage du debug DSP
 *
 *  ============================================================================
 */

Void LogCallback (Uint32 event, Pvoid arg, Pvoid info)
{
  static char PrintBuffer[512];

  // get data from DSP:
  int status = PROC_read(0, (int)info, 512, PrintBuffer);

  if (status == DSP_SOK)
    printf ("DSP-LOG: %s", PrintBuffer);
  else
    printf ("DEBUG: Unable to read dsp-mem %p\n", info);

  // notify the DSP-side:
  NOTIFY_notify (0, 0, 6, 0);

}



/** ============================================================================
 *  fonctions OpenCV
 *
 *  ============================================================================
 */

NORMAL_API
Void
dspCvtColor(IN IplImage* img_src,IN IplImage* img_dst, int code )
{
    DSPCV_query* outMsg;
    struct CvtColor_msg *message;
    CMEM_AllocParams params;
    params = CMEM_DEFAULTPARAMS;
    params.flags = CMEM_NONCACHED;
    params.type=CMEM_HEAP;
    char *data_img_src_virt,*data_img_dst_virt;

    MESSAGE_0Print ("Entered dspCvtColor()\n") ;

    // allocate processing request
    status = MSGQ_alloc(SAMPLE_POOL_ID, APP_OUT_BUFFER_SIZE, (MSGQ_Msg*) &outMsg);
    if (DSP_FAILED(status)) printf("MSGQ_alloc() failed.  Status = 0x%X\n", status);

    CMEM_cacheWb((char*)img_src->imageData,img_src->imageSize);

    data_img_src_virt=img_src->imageData;//sauvegarde adresse virtuelle
    data_img_dst_virt=img_dst->imageData;
    img_src->imageData = (char*)CMEM_getPhys(img_src->imageData);//remplace par adresse physique
    img_dst->imageData = (char*)CMEM_getPhys(img_dst->imageData);
    CMEM_cacheWb(img_src,sizeof(IplImage));
    CMEM_cacheWb(img_dst,sizeof(IplImage));

    message =(CvtColor_msg*)CMEM_alloc(sizeof(CvtColor_msg),&params);
    message->img_src = (IplImage*)CMEM_getPhys(img_src);
    message->img_dst = (IplImage*)CMEM_getPhys(img_dst);
    message->code=(int32_t)code;
    outMsg->openCVFxnIndex=CV_RGB_TO_GRAYSCALE;
    outMsg->msg_ptr=CMEM_getPhys(message);

    if (DSP_SUCCEEDED (status)) status=MESSAGE_Execute(outMsg) ;

    img_src->imageData = data_img_src_virt;//restaure adresse virtuelle
    img_dst->imageData = data_img_dst_virt;

    CMEM_cacheInv((char*)img_dst->imageData,img_dst->imageSize);
    if (CMEM_free(message, &params) < 0) printf("Failed to free buffer at %#x\n",(unsigned int) message);

    MESSAGE_0Print ("Leaving dspCvtColor()\n") ;
}


NORMAL_API
Void
dspCloneImage(IN IplImage* img_src,IN IplImage* img_dst)
{
    DSPCV_query* outMsg;
    struct CloneImage_msg *message;
    CMEM_AllocParams params;
    params = CMEM_DEFAULTPARAMS;
    params.flags = CMEM_NONCACHED;
    params.type=CMEM_HEAP;
    char *data_img_src_virt,*data_img_dst_virt;

    MESSAGE_0Print ("Entered dspCloneImage()\n") ;

    // allocate processing request
    status = MSGQ_alloc(SAMPLE_POOL_ID, APP_OUT_BUFFER_SIZE, (MSGQ_Msg*) &outMsg);
    if (DSP_FAILED(status)) MESSAGE_1Print("MSGQ_alloc() failed.  Status = 0x%X\n", status);

    CMEM_cacheWb((char*)img_src->imageData,img_src->imageSize);
    data_img_src_virt=img_src->imageData;//sauvegarde adresse virtuelle
    data_img_dst_virt=img_dst->imageData;
    img_src->imageData = (char*)CMEM_getPhys(img_src->imageData);//remplace par adresse physique
    img_dst->imageData = (char*)CMEM_getPhys(img_dst->imageData);
    CMEM_cacheWb(img_src,sizeof(IplImage));
    CMEM_cacheWb(img_dst,sizeof(IplImage));

    message =(CloneImage_msg*)CMEM_alloc(sizeof(CloneImage_msg),&params);
    message->img_src = (IplImage*)CMEM_getPhys(img_src);
    message->img_dst = (IplImage*)CMEM_getPhys(img_dst);

    outMsg->openCVFxnIndex=CV_CLONE_IMAGE;
    outMsg->msg_ptr=CMEM_getPhys(message);

    if (DSP_SUCCEEDED (status)) status=MESSAGE_Execute(outMsg) ;

    img_src->imageData = data_img_src_virt;//restaure adresse virtuelle
    img_dst->imageData = data_img_dst_virt;

    CMEM_cacheInv((char*)img_dst->imageData,img_dst->imageSize);
    if (CMEM_free(message, &params) < 0) MESSAGE_1Print("Failed to free buffer at %#x\n",(unsigned int) message);

    MESSAGE_0Print ("Leaving dspCloneImage()\n") ;
}

NORMAL_API
Void
dspSobel(IN IplImage* img_src,IN IplImage* img_dst,int dx,int dy)
{
    DSPCV_query* outMsg;
    struct Sobel_msg *message;
    CMEM_AllocParams params;
    params = CMEM_DEFAULTPARAMS;
    params.flags = CMEM_NONCACHED;
    params.type=CMEM_HEAP;
    char *data_img_src_virt,*data_img_dst_virt;

    MESSAGE_0Print ("Entered dspSobel()\n") ;

    // allocate processing request
    status = MSGQ_alloc(SAMPLE_POOL_ID, APP_OUT_BUFFER_SIZE, (MSGQ_Msg*) &outMsg);
    if (DSP_FAILED(status)) printf("MSGQ_alloc() failed.  Status = 0x%X\n", status);

    CMEM_cacheWb((char*)img_src->imageData,img_src->imageSize);
    data_img_src_virt=img_src->imageData;//sauvegarde adresse virtuelle
    data_img_dst_virt=img_dst->imageData;
    img_src->imageData = (char*)CMEM_getPhys(img_src->imageData);//remplace par adresse physique
    img_dst->imageData = (char*)CMEM_getPhys(img_dst->imageData);
    CMEM_cacheWb(img_src,sizeof(IplImage));
    CMEM_cacheWb(img_dst,sizeof(IplImage));

    message =(Sobel_msg*)CMEM_alloc(sizeof(Sobel_msg),&params);
    message->img_src = (IplImage*)CMEM_getPhys(img_src);
    message->img_dst = (IplImage*)CMEM_getPhys(img_dst);
    message->dx=dx;
    message->dy=dy;
    outMsg->openCVFxnIndex=CV_SOBEL;
    outMsg->msg_ptr=CMEM_getPhys(message);

    if (DSP_SUCCEEDED (status)) status=MESSAGE_Execute(outMsg);

    img_src->imageData = data_img_src_virt;//restaure adresse virtuelle
    img_dst->imageData = data_img_dst_virt;

    CMEM_cacheInv((char*)img_dst->imageData,img_dst->imageSize);
    if (CMEM_free(message, &params) < 0) printf("Failed to free buffer at %#x\n",(unsigned int) message);

    MESSAGE_0Print ("Leaving dspSobel()\n") ;
}



//#if defined (__cplusplus)
//}
//#endif /* defined (__cplusplus) */
