#include <stdio.h>
#include <cxcore.h>
#include <highgui.h>
//Allignement a verifier
#define  CMEM_MALLOC_ALIGN    32


void* icvCMEMAlloc( size_t size, void* userdata);
int icvCMEMFree( void* ptr, void* userdata);

