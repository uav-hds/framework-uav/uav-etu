//  created:    2012/01/18
//  filename:   basic_image.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 6599
//
//  version:    $Id: $
//
//  purpose:    copie et conversion de couleurs
//
//
/*********************************************************************/

#ifndef BASIC_IMAGE_H
#define BASIC_IMAGE_H

#include "cxtypes.h"
#include <stdint.h>

#if defined OMAPL138 || defined OMAP3530

#else
#include "typedef.h"//pour const et restrict, a deplacer
#endif

void dspCloneImage(IplImage* img_src,IplImage* img_dst);

void dspCvtColor(IplImage* img_src,IplImage* img_dst, int32_t code);
inline void convert_BGR2GRAY(int8_t* restrict dest,const int8_t* restrict src, uint32_t n);
inline void convert_YUYV2GRAY(int8_t* restrict dest,const int8_t* restrict src, uint32_t n);


#endif /*BASIC_IMAGE_H*/
