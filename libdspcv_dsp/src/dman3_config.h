/*
 *  Guillaume Sanahuja: modifications pour utiliser 2 canaux
 */

#ifndef DMAN3_CONFIG_H
#define DMAN3_CONFIG_H

#include <std.h>
#include <ti/xdais/idma3.h>
#include <ti/sdo/fc/dman3/dman3.h>
#include <ti/sdo/fc/acpy3/acpy3.h>

void dman3_start(IDMA3_Handle *h_in,IDMA3_Handle *h_out);
void dman3_stop(IDMA3_Handle *h_in,IDMA3_Handle *h_out);

#endif /*DMAN3_CONFIG_H*/
