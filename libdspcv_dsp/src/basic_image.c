//  created:    2012/01/18
//  filename:   basic_image.c
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 6599
//
//  version:    $Id: $
//
//  purpose:    copie et conversion de couleurs
//
//
/*********************************************************************/

#include "basic_image.h"
#include <stdlib.h>

#if defined OMAPL138 || defined OMAP3530
#include <std.h>
#include <ti/sdo/fc/acpy3/acpy3.h>
#define _ACPY3_fastConfigure32b(handle,type,addr,id) ACPY3_fastConfigure32b(handle,type,(uint32_t)addr,id)
//faire un header
extern void _printf(const char * format, ...);
#else
#include <string.h>
#include "C64intrins.h"
#include "wrappers.h"
#define _ACPY3_fastConfigure32b(handle,type,addr,id) ACPY3_fastConfigure_ptr(handle,type,(void*)addr,id)
#endif

#include "opencv_common.h"

#ifdef OMAPL138
extern int32_t DDR;
extern int32_t IRAM;
extern int32_t L1DSRAM;
extern int32_t CACHE_L1D;
#define BUF_SEG_ID IRAM
//#define BUF_SEG_ID L1DSRAM
#define BUF_ALIGN 128
#endif


#ifdef OMAP3530
extern int32_t DDR2;
extern int32_t L1DSRAM;
#define BUF_ALIGN 128
#endif

#define BUFF_SIZE (1024*39) //doit etre un multiple de 64

//les copies DMA sont plus rapide sur une taille multiple de 8
#define COPY_SIZE (65528) //doit etre un multiple de 8

extern IDMA3_Handle h_in,h_out;


void dspCloneImage(IplImage* img_src,IplImage* img_dst)
{
    ACPY3_Params p;
    uint32_t i,max_iterations;
    uint8_t *data_src=(uint8_t *)img_src->imageData;
    uint8_t *data_dst=(uint8_t *)img_dst->imageData;

    max_iterations=(uint32_t)((img_src->imageSize)/COPY_SIZE);

    p.transferType = ACPY3_1D1D;
    p.dstAddr = (void *)data_dst;
    p.srcAddr = (void *)data_src;
    p.elementSize = COPY_SIZE;
    p.srcElementIndex = 1;
    p.dstElementIndex = 1;
    p.numElements = 1;
    p.numFrames = 1;
    p.waitId = 0;// waitId of 0 implies wait after the first transfer
    ACPY3_configure(h_out, &p, 0);

    for(i=0;i<max_iterations;i++)
    {
        ACPY3_start(h_out);
        data_src+=COPY_SIZE;
        data_dst+=COPY_SIZE;

        ACPY3_wait(h_out);
        _ACPY3_fastConfigure32b(h_out,ACPY3_PARAMFIELD_SRCADDR,data_src,0);
        _ACPY3_fastConfigure32b(h_out,ACPY3_PARAMFIELD_DSTADDR,data_dst,0);
    }
    //traitement du rebus (ou si l'image est plus petite que COPY_SIZE)
    if(img_src->imageSize!=COPY_SIZE*max_iterations)
    {
        ACPY3_fastConfigure16b(h_out,ACPY3_PARAMFIELD_ELEMENTSIZE,(uint16_t)(img_src->imageSize-COPY_SIZE*max_iterations),0);
        ACPY3_start(h_out);
        ACPY3_wait(h_out);
    }

}

void dspCvtColor(IplImage* img_src,IplImage* img_dst, int32_t code)
{

    ACPY3_Params p;
    uint32_t i;
    int8_t* cache_in_old;
    int8_t* cache_in;
    int8_t* cache_out_old;
    int8_t* cache_out;
    int8_t* data_src=(int8_t*)img_src->imageData;
    int8_t* data_dst=(int8_t*)img_dst->imageData;
    int8_t* cache_tmp;

    if(code==DSP_BGR2GRAY)
    {

        cache_in_old=MEM_alloc(L1DSRAM, 3*img_src->width, BUF_ALIGN);
        cache_in=MEM_alloc(L1DSRAM, 3*img_src->width, BUF_ALIGN);
        cache_out_old=MEM_alloc(L1DSRAM,img_src->width, BUF_ALIGN);
        cache_out=MEM_alloc(L1DSRAM, img_src->width, BUF_ALIGN);

        if(cache_in_old==NULL || cache_in==NULL || cache_out_old==NULL || cache_out==NULL)
        {
            _printf("erreur allocation dspCvtColor\n");
        }

        p.transferType = ACPY3_1D1D;
        p.elementSize = 3*img_src->width;
        p.srcElementIndex = 1;
        p.dstElementIndex = 1;
        p.numElements = 1;
        p.numFrames = 1;
        p.waitId = 0;// waitId of 0 implies wait after the first transfer
        ACPY3_configure(h_in, &p, 0);
        ACPY3_configure(h_out, &p, 0);
        ACPY3_fastConfigure16b(h_out,ACPY3_PARAMFIELD_ELEMENTSIZE,img_src->width,0);

        //les 2 premieres iterations ne sortent rien (+2)
        for(i=0;i<img_src->height+2;i++)
        {
            //cache_out_old > dst	trait cache_in > cache_out  src > cache_in_old
            if(i>1)
            {
                _ACPY3_fastConfigure32b(h_out,ACPY3_PARAMFIELD_SRCADDR,cache_out_old,0);
                _ACPY3_fastConfigure32b(h_out,ACPY3_PARAMFIELD_DSTADDR,data_dst,0);
                ACPY3_start(h_out);
                data_dst+=img_src->width;
            }

            _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_SRCADDR,data_src,0);
            _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_DSTADDR,cache_in_old,0);
            ACPY3_start(h_in);
            data_src+=3*img_src->width;

            //traitement non necessaire quand i=0 et à la fin, on le fait qd meme pour enlever un if...
            //car traitement + rapide que la copie
            convert_BGR2GRAY(cache_out,cache_in,img_src->width);

            ACPY3_wait(h_out);
            ACPY3_wait(h_in);

            //permutation circulaire
            cache_tmp=cache_in;
            cache_in=cache_in_old;
            cache_in_old=cache_tmp;

            cache_tmp=cache_out;
            cache_out=cache_out_old;
            cache_out_old=cache_tmp;
        }

        MEM_free(L1DSRAM, cache_in_old,3*img_src->width);
        MEM_free(L1DSRAM, cache_in,3*img_src->width);
        MEM_free(L1DSRAM, cache_out_old,img_src->width);
        MEM_free(L1DSRAM, cache_out,img_src->width);

    }

    if(code==DSP_YUYV2GRAY)
    {
        cache_in_old=(int8_t*)MEM_alloc(L1DSRAM, 2*img_src->width, BUF_ALIGN);
        cache_in=(int8_t*)MEM_alloc(L1DSRAM, 2*img_src->width, BUF_ALIGN);
        cache_out_old=(int8_t*)MEM_alloc(L1DSRAM,img_src->width, BUF_ALIGN);
        cache_out=(int8_t*)MEM_alloc(L1DSRAM, img_src->width, BUF_ALIGN);

        if(cache_in_old==NULL || cache_in==NULL || cache_out_old==NULL || cache_out==NULL)
        {
            _printf("erreur allocation dspCvtColor\n");
        }

        p.transferType = ACPY3_1D1D;
        p.elementSize = 2*img_src->width;
        p.srcElementIndex = 1;
        p.dstElementIndex = 1;
        p.numElements = 1;
        p.numFrames = 1;
        p.waitId = 0;// waitId of 0 implies wait after the first transfer
        ACPY3_configure(h_in, &p, 0);
        ACPY3_configure(h_out, &p, 0);
        ACPY3_fastConfigure16b(h_out,ACPY3_PARAMFIELD_ELEMENTSIZE,img_src->width,0);

        //les 2 premieres iterations ne sortent rien (+2)
        for(i=0;i<img_src->height+2;i++)
        {
            //cache_out_old > dst	trait cache_in > cache_out  src > cache_in_old
            if(i>1)
            {
                _ACPY3_fastConfigure32b(h_out,ACPY3_PARAMFIELD_SRCADDR,cache_out_old,0);
                _ACPY3_fastConfigure32b(h_out,ACPY3_PARAMFIELD_DSTADDR,data_dst,0);
                ACPY3_start(h_out);
                data_dst+=img_src->width;
            }

            _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_SRCADDR,data_src,0);
            _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_DSTADDR,cache_in_old,0);
            ACPY3_start(h_in);
            data_src+=2*img_src->width;

            //traitement non necessaire quand i=0, on le fait qd meme pour enlever un if...
            //car traitement + rapide que la copie
            convert_YUYV2GRAY(cache_out,cache_in,img_src->width);

            ACPY3_wait(h_out);
            ACPY3_wait(h_in);

            //permutation circulaire
            cache_tmp=cache_in;
            cache_in=cache_in_old;
            cache_in_old=cache_tmp;

            cache_tmp=cache_out;
            cache_out=cache_out_old;
            cache_out_old=cache_tmp;
        }

        MEM_free(L1DSRAM, cache_in_old,2*img_src->width);
        MEM_free(L1DSRAM, cache_in,2*img_src->width);
        MEM_free(L1DSRAM, cache_out_old,img_src->width);
        MEM_free(L1DSRAM, cache_out,img_src->width);
    }

}


inline void convert_BGR2GRAY(int8_t* restrict dest,const int8_t* restrict src, uint32_t n)
{
    uint32_t i,pix,y;

    for (i=0; i<n; i++)
    {
        pix=_mem4_const(src);
        src+=3;
        y=_dotpu4(0x004d971c,pix);
        // undo the scale by 256 and write to memory:
        *dest++ = (y>>8);
    }
}

inline void convert_YUYV2GRAY(int8_t* restrict dest,const int8_t* restrict src, uint32_t n)
{
    uint32_t i;
    for (i=0; i<n; i+=2)
    {
        *dest=*src;
        *src++;//U
        *src++;//Y
        *dest++;

        *dest=*src;
        *src++;//V
        *src++;//Y
        *dest++;
    }
}
