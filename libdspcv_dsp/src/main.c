/** ============================================================================
 *  @file   main.c
 *
 *  @path   $(DSPLINK)/dsp/src/samples/message/
 *
 *  @desc   Main function that calls SWI or TSK message applications based
 *          on the parameter TSK_MODE.
 *
 *  @ver    1.63
 *  ============================================================================
 *  Copyright (C) 2002-2009, Texas Instruments Incorporated -
 *  http://www.ti.com/
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *  *  Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *  *  Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *  *  Neither the name of Texas Instruments Incorporated nor the names of
 *     its contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 *  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 *  PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 *  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 *  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ============================================================================
 */

/*
 * Guillaume Sanahuja: quelques modifs pour le printf
 */

/*  ----------------------------------- DSP/BIOS Headers            */
#include <std.h>
#include <sys.h>
#include <sem.h>
#include <tsk.h>
#include <msgq.h>
#include <pool.h>

/*  ----------------------------------- DSP/BIOS LINK Headers       */
#include <dsplink.h>
//#include <failure.h>


/*  ----------------------------------- Sample Headers              */
#include "tskMessage.h"
#include <bcache.h>

#ifdef __cplusplus
extern "C" {
#endif


/** ----------------------------------------------------------------------------
 *  @func   tskMessage
 *
 *  @desc   Task for TSK based TSKMESSAGE application.
 *
 *  @arg    None
 *
 *  @ret    None
 *
 *  @enter  None
 *
 *  @leave  None
 *
 *  @see    None
 *  ----------------------------------------------------------------------------
 */
static Int tskMessage () ;


//fonctions pour le debug dsp
// need a semaphore for syncronization:
static SEM_Handle dprint_sema;

// buffer for data-transfer, L2 cache aligned.
#pragma DATA_ALIGN(printf_buffer, 128);
static char printf_buffer[512];

void _printf (const char * format, ...);
Void dprint_callback (Uint32 EventNo, Ptr arg, Ptr a_Info);

/** ============================================================================
 *  @func   main
 *
 *  @desc   Entry function.
 *
 *  @modif  None
 *  ============================================================================
 */
Void main (Int argc, Char * argv [])
{

    /* TSK based application */
    TSK_Handle                tskMessageTask ;

    // create the semaphore
    dprint_sema = SEM_create (0, NULL);


    /* Initialize DSP/BIOS LINK. */
    DSPLINK_init () ;

    // register callback from the gpp side:
    NOTIFY_register (ID_GPP, 0, 6, dprint_callback, 0);


    // Creating task for TSKMESSAGE application
    tskMessageTask = TSK_create (tskMessage, NULL, 0) ;
    if (tskMessageTask != NULL)
    {
        _printf("dsp ok\n");
    }
    else
    {
        _printf("Create TSKMESSAGE: Failed.\n");
    }


}


/** ----------------------------------------------------------------------------
 *  @func   tskMessage
 *
 *  @desc   Task for TSK based TSKMESSAGE application.
 *
 *  @modif  None
 *  ----------------------------------------------------------------------------
 */
static Int tskMessage()
{
    Int                       status = SYS_OK ;
    TSKMESSAGE_TransferInfo * info ;

    /* Create Phase */
    status = TSKMESSAGE_create (&info) ;

    /* Execute Phase */
    if (status == SYS_OK)
    {
        /* Start the execution phase. */
        status = TSKMESSAGE_execute(info) ;
        if (status != SYS_OK) _printf("TSKMESSAGE_execute: Failed.\n");
    }

    /* Delete Phase */
    status = TSKMESSAGE_delete (info) ;
    if (status != SYS_OK) _printf("TSKMESSAGE_delete: Failed.\n");
    return status ;
}

void _printf (const char * format, ...)
{
  int n;
  va_list args;
  va_start (args, format);
  n = vsprintf (printf_buffer,format, args);
  va_end (args);
  if (n<=0) return;

  // writeback cache:
  BCACHE_wb(printf_buffer, n, 1);
  // notify GPP:
  NOTIFY_notify (ID_GPP, 0, 6, (Uint32)printf_buffer);
  // wait for GPP acknowledge
  SEM_pendBinary (dprint_sema, SYS_FOREVER);

}

Void dprint_callback (Uint32 EventNo, Ptr arg, Ptr a_Info)
{
  // acknowledge gpp is done with reading the print-buffer:
  SEM_postBinary (dprint_sema);

}


#if defined (__cplusplus)
}
#endif /* defined (__cplusplus) */
