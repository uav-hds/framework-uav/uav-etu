if("${CMAKE_SYSTEM_PROCESSOR}" MATCHES "arm")
        SET(ARCH_DIR "arm")
else()
	if(WIN32)
		if(MINGW)
			if (CMAKE_SIZEOF_VOID_P MATCHES "8")#64 bits
				SET(ARCH_DIR "x86_64/win")
			else()#32 bits
				SET(ARCH_DIR "x86/win")
				ADD_DEFINITIONS(-D__MINGW__)
				#SET(LIBXML2_LIBRARIES $ENV{IGEP_ROOT}/uav_dev/lib/x86/win)
				#SET(LIBXML2_INCLUDE_DIR toto)
			endif()
		else()
			message(FATAL_ERROR,"seul le compilateur mingw est supporté")
	    	endif()
	else()	
		if (CMAKE_SIZEOF_VOID_P MATCHES "8")#64 bits
			SET(ARCH_DIR "x86_64/unix")
		else()#32 bits
			SET(ARCH_DIR "x86/unix")
		endif()
	endif()
endif()
