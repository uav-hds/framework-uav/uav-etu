include($ENV{IGEP_ROOT}/uav_dev/cmake_modules/arch_dir.cmake)

list(APPEND CMAKE_MODULE_PATH $ENV{IGEP_ROOT}/uav_dev/cmake_modules/)

#pour trouver les lib de la toolchain
if(NOT "${CMAKE_SYSTEM_PROCESSOR}" MATCHES "arm")
SET(CMAKE_FIND_ROOT_PATH $ENV{OECORE_NATIVE_SYSROOT})
endif()

#framework
SET(FRAMEWORK_USE_FILE $ENV{IGEP_ROOT}/uav_dev/cmake_modules/FrameworkUseFile.cmake)

#default executable ouput paths
if(NOT DEFINED EXECUTABLE_OUTPUT_PATH)
	SET(EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/bin)
endif()
if(NOT DEFINED TARGET_EXECUTABLE_OUTPUT_PATH)
	SET(TARGET_EXECUTABLE_OUTPUT_PATH bin/arm)
endif()

#read ssh config file and add tagets
if("${CMAKE_SYSTEM_PROCESSOR}" MATCHES "arm")
file (STRINGS $ENV{HOME}/.ssh/config TEST)
	foreach(f ${TEST})
		string(FIND ${f} "Host " POS)#cherche ligne host
		if(${POS} GREATER -1)
			string(REPLACE Host "" TARGET_NAME ${f})#enleve Host
			string(STRIP ${TARGET_NAME} TARGET_NAME)#enleve les espaces
		endif()
		string(FIND ${f} HostName POS)#cherche hostname
		if(${POS} GREATER -1)
			string(FIND ${f} "192.168.6." POS)#cherche addresse
			if(${POS} GREATER 0)#garde que les adresses en 192.168.6.x
				string(REPLACE HostName "" ADDRESS ${f})#enleve Hostname
				string(STRIP ${ADDRESS} ADDRESS)#enleve les espaces
				message("adding delivery target for " ${TARGET_NAME} " (" ${ADDRESS} ")")
				string(REPLACE "/" "_" TARGET_PATH ${TARGET_EXECUTABLE_OUTPUT_PATH})#les / ne sont pas acceptés
				add_custom_target(
				    delivery_${TARGET_NAME}_root@${ADDRESS}_${TARGET_PATH}_${PROJECT_NAME}
				    COMMAND make
				    COMMAND scp ${EXECUTABLE_OUTPUT_PATH}/${PROJECT_NAME} root@${ADDRESS}:${TARGET_EXECUTABLE_OUTPUT_PATH}
				)
			endif()
		endif()
	endforeach(f) 
endif()
