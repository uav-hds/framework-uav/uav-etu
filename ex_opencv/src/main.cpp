#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>

#include <cv.h>
#include <cxcore.h>
#include <highgui.h>
#include <dspcv_gpp.h>


#define ITERATIONS 100


void sobel_dsp(const char* imageA);
void sobel_cv(const char* imageA);

int main(int argc, char ** argv)
{
    if (argc != 2)
    {
        printf ("Usage : %s <absolute path of DSP executable>\n", argv [0]) ;
    }
    else
    {

        init_dsp(argv [1]) ;

        sobel_cv("imagesA.bmp");
        sobel_dsp("imagesA.bmp");


        //  Perform cleanup operation.
        close_dsp() ;

    }

}


void sobel_dsp(const char* imageA)
{
    struct timeval tvs, tve, tvd;

    printf("Sobel DSP\n");

    IplImage* cimg=cvLoadImage(imageA);
    IplImage* gimg=cvCreateImage(cvSize(cimg->width,cimg->height),IPL_DEPTH_8U,1);
    IplImage* sobel=cvCreateImage(cvSize(cimg->width,cimg->height),IPL_DEPTH_8U,1);

    gettimeofday(&tvs, NULL);
    for(int i=0;i<ITERATIONS;i++) dspCvtColor(cimg,gimg,CV_BGR2GRAY);
    gettimeofday(&tve, NULL); timersub(&tve, &tvs, &tvd);
    printf("%i iterations dspCvtColor, temps: %d.%06d\n", ITERATIONS,(int)tvd.tv_sec, (int)tvd.tv_usec);

    gettimeofday(&tvs, NULL);
    for(int i=0;i<ITERATIONS;i++) dspSobel(gimg,sobel,1,1);
    gettimeofday(&tve, NULL); timersub(&tve, &tvs, &tvd);
    printf("%i iterations dspSobel, temps: %d.%06d\n", ITERATIONS,(int)tvd.tv_sec, (int)tvd.tv_usec);

    if(!cvSaveImage("gris_dsp.bmp",gimg)) printf("Could not save.\n");
    if(!cvSaveImage("sobel_dsp.bmp",sobel)) printf("Could not save.\n");

    printf("\n");

    cvReleaseImage(&gimg);
    cvReleaseImage(&sobel);
    cvReleaseImage(&cimg);


}

void sobel_cv(const char* imageA)
{
    struct timeval tvs, tve, tvd;

    printf("Sobel ARM\n");

    IplImage* cimg=cvLoadImage(imageA);
    IplImage* gimg=cvCreateImage(cvSize(cimg->width,cimg->height),IPL_DEPTH_8U,1);
    IplImage* sobel=cvCreateImage(cvSize(cimg->width,cimg->height),IPL_DEPTH_8U,1);

    gettimeofday(&tvs, NULL);
    for(int i=0;i<ITERATIONS;i++) cvCvtColor(cimg,gimg,CV_BGR2GRAY);
    gettimeofday(&tve, NULL); timersub(&tve, &tvs, &tvd);
    printf("%i iterations cvCvtColor, temps: %d.%06d\n", ITERATIONS,(int)tvd.tv_sec, (int)tvd.tv_usec);

    gettimeofday(&tvs, NULL);
    for(int i=0;i<ITERATIONS;i++) cvSobel(gimg,sobel,1,1,3);
    gettimeofday(&tve, NULL); timersub(&tve, &tvs, &tvd);
    printf("%i iterations cvSobel, temps: %d.%06d\n", ITERATIONS,(int)tvd.tv_sec, (int)tvd.tv_usec);

    if(!cvSaveImage("gris_arm.bmp",gimg)) printf("Could not save.\n");
    if(!cvSaveImage("sobel_arm.bmp",sobel)) printf("Could not save.\n");

    printf("\n");

    cvReleaseImage(&gimg);
    cvReleaseImage(&sobel);
    cvReleaseImage(&cimg);
}
