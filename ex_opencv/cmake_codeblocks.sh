mkdir -p build
cd build
cmake ../ -G "CodeBlocks - Unix Makefiles"
cd ..
mkdir -p build_arm
cd build_arm
if [ -z "$1" ]; then
	CMAKE_CROSS_TOOLCHAIN=/opt/poky/toolchain.cmake
else
	CMAKE_CROSS_TOOLCHAIN=$1
fi
cmake ../ -G "CodeBlocks - Unix Makefiles" -DCMAKE_TOOLCHAIN_FILE=${CMAKE_CROSS_TOOLCHAIN}
