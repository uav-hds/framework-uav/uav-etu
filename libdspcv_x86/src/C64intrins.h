/* ***********************************************************
* THIS PROGRAM IS PROVIDED "AS IS". TI MAKES NO WARRANTIES OR
* REPRESENTATIONS, EITHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
* FOR A PARTICULAR PURPOSE, LACK OF VIRUSES, ACCURACY OR
* COMPLETENESS OF RESPONSES, RESULTS AND LACK OF NEGLIGENCE.
* TI DISCLAIMS ANY WARRANTY OF TITLE, QUIET ENJOYMENT, QUIET
* POSSESSION, AND NON-INFRINGEMENT OF ANY THIRD PARTY
* INTELLECTUAL PROPERTY RIGHTS WITH REGARD TO THE PROGRAM OR
* YOUR USE OF THE PROGRAM.
*
* IN NO EVENT SHALL TI BE LIABLE FOR ANY SPECIAL, INCIDENTAL,
* CONSEQUENTIAL OR INDIRECT DAMAGES, HOWEVER CAUSED, ON ANY
* THEORY OF LIABILITY AND WHETHER OR NOT TI HAS BEEN ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGES, ARISING IN ANY WAY OUT
* OF THIS AGREEMENT, THE PROGRAM, OR YOUR USE OF THE PROGRAM.
* EXCLUDED DAMAGES INCLUDE, BUT ARE NOT LIMITED TO, COST OF
* REMOVAL OR REINSTALLATION, COMPUTER TIME, LABOR COSTS, LOSS
* OF GOODWILL, LOSS OF PROFITS, LOSS OF SAVINGS, OR LOSS OF
* USE OR INTERRUPTION OF BUSINESS. IN NO EVENT WILL TI'S
* AGGREGATE LIABILITY UNDER THIS AGREEMENT OR ARISING OUT OF
* YOUR USE OF THE PROGRAM EXCEED FIVE HUNDRED DOLLARS
* (U.S.$500).
*
* Unless otherwise stated, the Program written and copyrighted
* by Texas Instruments is distributed as "freeware".  You may,
* only under TI's copyright in the Program, use and modify the
* Program without any charge or restriction.  You may
* distribute to third parties, provided that you transfer a
* copy of this license to the third party and the third party
* agrees to these terms by its first use of the Program. You
* must reproduce the copyright notice and any other legend of
* ownership on each copy or partial copy, of the Program.
*
* You acknowledge and agree that the Program contains
* copyrighted material, trade secrets and other TI proprietary
* information and is protected by copyright laws,
* international copyright treaties, and trade secret laws, as
* well as other intellectual property laws.  To protect TI's
* rights in the Program, you agree not to decompile, reverse
* engineer, disassemble or otherwise translate any object code
* versions of the Program to a human-readable form.  You agree
* that in no event will you alter, remove or destroy any
* copyright notice included in the Program.  TI reserves all
* rights not specifically granted under this license. Except
* as specifically provided herein, nothing in this agreement
* shall be construed as conferring by implication, estoppel,
* or otherwise, upon you, any license or other right under any
* TI patents, copyrights or trade secrets.
*
* You may not use the Program in non-TI devices.
* ********************************************************* */
/***************************************************************************/
/* Texas Instruments, Stafford, TX                                         */
/*									   */
/* NAME			C64intrins.h					   */
/*									   */
/* REVISION		C64intrins.h					   */
/*									   */
/* DESCRIPTION	Header file for C64intrins.c            		   */
/***************************************************************************/

#define MIN_16 -32768
#define MAX_16  32767
#define MAX_INT  (Word32)0x7fffffff
#define MIN_INT  (Word32)0x80000000

typedef char Word8;
typedef unsigned char UWord8;
typedef short Word16;
typedef unsigned short UWord16;
typedef int Word32;
typedef unsigned int UWord32;
typedef long Word40;
typedef unsigned long UWord40;
typedef unsigned long long UWord64;
typedef long long Word64;

Word32 abs2_c(Word32 src);
Word32 add2_c(Word32 src1, Word32 src2);
Word32 add4_c(Word32 src1, Word32 src2);
UWord32 amem4_const_c(void* ptr);
Word32 avg2_c(Word32 src1, Word32 src2);
Word32 avgu4_c(UWord32 src1, UWord32 src2);
UWord32 bitc4_c(UWord32 src);
UWord32 bitr_c(UWord32 src);
Word32 cmpeq2_c(Word32 src1, Word32 src2);
Word32 cmpeq4_c(Word32 src1, Word32 src2);
Word32 cmpgt2_c(Word32 src1, Word32 src2);
UWord32 cmpgtu4_c(UWord32 src1, UWord32 src2);
UWord32 deal_c(UWord32 src);
Word32 dotp2_c(Word32 src1, Word32 src2);
Word40 ldotp2_c(Word32 src1, Word32 src2);
Word32 dotpn2_c(Word32 src1, Word32 src2);
Word32 dotpnrsu2_c(Word32 src1, UWord32 src2);
Word32 dotprsu2_c(Word32 src1, UWord32 src2);
Word32 dotpsu4_c(Word32 src1, UWord32 src2);
UWord32 dotpu4_c(UWord32 src1, UWord32 src2);
Word64 ddotpl2_c(Word64 src1, UWord32 src2);
Word32 ext_c(Word32 src2,UWord32 csta,UWord32 cstb);
UWord32 extu_c(UWord32 src2,UWord32 csta,UWord32 cstb);
UWord32 ftoi_c(float value);
UWord32 gmpy(UWord8 src1, UWord8 src2, UWord32 gsize, UWord32 gpoly);
UWord32 gmpy4_c(UWord32 src1, UWord32 src2);
double itod_c(UWord32 src2, UWord32 src1);
UWord64 itoll_c(UWord32 src2, UWord32 src1);
UWord32 hi_c(double value);
UWord32 lo_c(double value);
UWord32 hill_c(UWord64 value);
UWord32 loll_c(UWord64 value);
Word32 max2_c(Word32 src1, Word32 src2);
UWord32 maxu4_c(UWord32 src1, UWord32 src2);
UWord32 mem4_const_c(void* ptr);
UWord64 mem8_const_c(void* ptr);
double memd8_const_c(void* ptr);
Word32 min2_c(Word32 src1, Word32 src2);
UWord32 minu4_c(UWord32 src1, UWord32 src2);
Word64 mpy32ll_c(Word32 src1, Word32 src2);
Word64 mpy2_c(Word32 src1, Word32 src2);
double mpy2_wrapper(Word32 x, Word32 y);
Word64 mpyhi_c(Word32 src1, Word32 src2);
double mpyhi_wrapper(Word32 src1, Word32 src2);
Word64 mpyli_c(Word32 src1, Word32 src2);
double mpyli_wrapper(Word32 src1, Word32 src2);
Word32 mpyhir_c(Word32 src1, Word32 src2);
Word32 mpylir_c(Word32 src1, Word32 src2);
Word64 mpysu4_c(Word32 src1, UWord32 src2);
double mpysu4_wrapper(Word32 src1, UWord32 src2);
UWord64 mpyu4_c(UWord32 src1, UWord32 src2);
double mpyu4_wrapper(UWord32 src1, UWord32 src2);
Word32 mvd_c(Word32 src);
Word32 nround_c(double value);
UWord32 pack2_c(UWord32 src1, UWord32 src2);
UWord32 packh2_c(UWord32 src1, UWord32 src2);
UWord32 packh4_c(UWord32 src1, UWord32 src2);
UWord32 packl4_c(UWord32 src1, UWord32 src2);
UWord32 packhl2_c(UWord32 src1, UWord32 src2);
UWord32 packlh2_c(UWord32 src1, UWord32 src2);
UWord32 rotl_c(UWord32 src1, UWord32 src2);
Word32 sadd_c(Word32 src1, Word32 src2);
Word32 sadd2_c(Word32 src1, Word32 src2);
Word32 saddus2_c(UWord32 src1, Word32 src2);
UWord32 saddu4_c(UWord32 src1, UWord32 src2);
UWord32 shfl_c(UWord32 src);
UWord32 shlmb_c(UWord32 src1, UWord32 src2);
UWord32 shrmb_c(UWord32 src1, UWord32 src2);
Word32 shr2_c(Word32 src1, UWord32 src2);
UWord32 shru2_c(UWord32 src1, UWord32 src2);
Word64 smpy2_c(Word32 src1, Word32 src2);
double smpy2_wrapper(Word32 x, Word32 y);
Word32 spack2_c(Word32 src1, Word32 src2);
UWord32 spacku4_c(Word32 src1, Word32 src2);
Word32 sshvl_c(Word32 src1, Word32 src2);
Word32 sshvr_c(Word32 src1, Word32 src2);
Word32 sub2_c(Word32 src1, Word32 src2);
Word32 sub4_c(Word32 src1, Word32 src2);
Word32 subabs4_c(Word32 src1, Word32 src2);
UWord32 swap4_c(UWord32 src);
UWord32 unpkhu4_c(UWord32 src);
UWord32 unpklu4_c(UWord32 src);
UWord32 xpnd2_c(UWord32 src);
UWord32 xpnd4_c(UWord32 src);

#define _abs2 		abs2_c
#define _add2		add2_c
#define _add4		add4_c
#define _amem4_const   amem4_const_c
#define _avg2		avg2_c
#define _avgu4		avgu4_c
#define _bitc4		bitc4_c
#define _bitr		bitr_c
#define _cmpeq2		cmpeq2_c
#define _cmpeq4		cmpeq4_c
#define _cmpgt2		cmpgt2_c
#define _cmpgtu4	cmpgtu4_c
#define _deal		deal_c
#define _dotp2		dotp2_c
#define _ldotp2		ldotp2_c
#define _dotpn2		dotpn2_c
#define _dotpnrsu2	dotpnrsu2_c
#define _dotprsu2	dotprsu2_c
#define _dotpsu4	dotpsu4_c
#define _dotpu4		dotpu4_c
#define _ddotpl2    ddotpl2_c
#define _ext       ext_c
#define _extu       extu_c
#define _ftoi       ftoi_c
#define _gmpy4		gmpy4_c
#define _hi         hi_c
#define _lo         lo_c
#define _hill       hill_c
#define _loll       loll_c
#define _itod       itod_c
#define _itoll      itoll_c
#define _max2		max2_c
#define _maxu4		maxu4_c
#define _mem4_const mem4_const_c
#define _amem8_const mem8_const_c //rajouter verif de l'allignement?
#define _mem8_const mem8_const_c
#define _memd8_const memd8_const_c
#define _min2		min2_c
#define _minu4		minu4_c
#define _mpy32ll    mpy32ll_c
#define _mpy2		mpy2_wrapper
#define _mpy2ll		mpy2_c
#define _mpyhi		mpyhi_wrapper
#define _mpyli		mpyli_wrapper
#define _mpyhir		mpyhir_c
#define _mpylir		mpylir_c
#define _mpysu4		mpysu4_c
#define _mpysu4ll		mpysu4_c
#define _mpyu4		mpyu4_c
#define _mpyu4ll	mpyu4_c
#define _mvd		mvd_c
#define _nround     nround_c
#define _pack2		pack2_c
#define _packh2		packh2_c
#define _packh4		packh4_c
#define _packl4		packl4_c
#define _packhl2	packhl2_c
#define _packlh2	packlh2_c
#define _rotl		rotl_c
#define _sadd		sadd_c
#define _sadd2		sadd2_c
#define _saddus2	saddus2_c
#define _saddu4		saddu4_c
#define _shfl		shfl_c
#define _shlmb		shlmb_c
#define _shrmb		shrmb_c
#define _shr2		shr2_c
#define _shru2		shru2_c
#define _smpy2		smpy2_wrapper
#define _spack2		spack2_c
#define _spacku4	spacku4_c
#define _sshvl		sshvl_c
#define _sshvr		sshvr_c
#define _sub2		sub2_c
#define _sub4		sub4_c
#define _subabs4	subabs4_c
#define _swap4		swap4_c
#define _unpkhu4	unpkhu4_c
#define _unpklu4	unpklu4_c
#define _xpnd2		xpnd2_c
#define _xpnd4		xpnd4_c


#define _nassert(a)
