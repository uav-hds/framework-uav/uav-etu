#ifndef WRAPPERS_H
#define WRAPPERS_H

#include "typedef.h"
#include <stdio.h>

#define _printf printf
#define TRUE 1
#define FALSE 0

void init_dsp(char* dspExecutable) ;
void close_dsp(void) ;

void* MEM_alloc(int seg_id, int size,int seg_align);
void* MEM_valloc(int seg_id, int size,int seg_align,char value);
void MEM_free(int seg_id,void* buf, int size);

void ACPY3_configure(IDMA3_Handle handle, ACPY3_Params *params,short transferNo);
void ACPY3_fastConfigure16b(IDMA3_Handle handle, ACPY3_ParamField16b fieldId,
    unsigned short value, short transferNo);
void ACPY3_fastConfigure32b(IDMA3_Handle handle, ACPY3_ParamField32b fieldId,
    unsigned int value, short transferNo);
void ACPY3_fastConfigure_ptr(IDMA3_Handle handle, ACPY3_ParamField32b fieldId,
    void* ptr, short transferNo);
inline void ACPY3_start(IDMA3_Handle handle);
void ACPY3_wait(IDMA3_Handle handle);

void BCACHE_setMar(void* baseAddr, size_t byteSize,int value);
void BCACHE_wb(void* blockPtr, size_t byteCnt, int wait);

#endif // WRAPPERS_H
