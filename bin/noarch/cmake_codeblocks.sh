DIR=build_$(uname -m)
#recupere cmake
if [ -f  ${OECORE_NATIVE_SYSROOT}/usr/bin/cmake ]; then
	CMAKE_NATIVE=${OECORE_NATIVE_SYSROOT}/usr/bin/cmake
else
	echo "cross toolchain is not installed, please read https://devel.hds.utc.fr/projects/igep/wiki/toolchain/install"
exit 1
fi

#verifie l'existence du lien symbolique build
if [ -d build ];then
	if ! readlink build > /dev/null ; then
		#c'est un répertoire, on quitte pour ne rien effacer
		echo "Erreur: build existe et est un répertoire; c'est sensé être un lien symbolique."
		exit 1
	fi
	if ! readlink build | grep -w $DIR >/dev/null; then
		echo "Attention, build pointait vers un autre répertoire."
	fi
	rm build
fi

#creation du repertoire
mkdir -p $DIR
#creation du lien symbolique
ln -s $DIR build

#creation project x86
cd build
rm -f CMakeCache.txt
cmake ../ -G "CodeBlocks - Unix Makefiles"

#creation projet ARM
cd ..
mkdir -p build_arm
cd build_arm
rm -f CMakeCache.txt
if [ -z "$1" ]; then
	CMAKE_CROSS_TOOLCHAIN=${OECORE_CMAKE_CROSS_TOOLCHAIN}
else
	CMAKE_CROSS_TOOLCHAIN=$1
fi
$CMAKE_NATIVE ../ -G "CodeBlocks - Unix Makefiles" -DCMAKE_TOOLCHAIN_FILE=${CMAKE_CROSS_TOOLCHAIN}
