#include <cxtypes.h>

//codes conversions couleurs
#define DSP_BGR2GRAY 6
#define DSP_YUYV2GRAY 62


#if defined (__cplusplus)
extern "C" {
#endif /* defined (__cplusplus) */


void init_dsp(char* dspExecutable) ;
void close_dsp(void) ;

void dspCvtColor(IplImage* img_src,IplImage* img_dst,int code);

void dspCloneImage(IplImage* img_src,IplImage* img_dst);

void dspSobel(IplImage* src_img,IplImage* dst_img,int dx,int dy);


#if defined (__cplusplus)
}
#endif /* defined (__cplusplus) */


